import request from '@/utils/request'

// 创建营养师个人资料
export function createProfile(data) {
  return request({
    url: '/dietician/profile/create',
    method: 'post',
    data: data
  })
}

// 更新营养师个人资料
export function updateProfile(data) {
  return request({
    url: '/dietician/profile/update',
    method: 'put',
    data: data
  })
}

// 删除营养师个人资料
export function deleteProfile(id) {
  return request({
    url: '/dietician/profile/delete?id=' + id,
    method: 'delete'
  })
}

// 获得营养师个人资料
export function getProfile(id) {
  return request({
    url: '/dietician/profile/get?id=' + id,
    method: 'get'
  })
}

// 获得营养师个人资料分页
export function getProfilePage(query) {
  return request({
    url: '/dietician/profile/page',
    method: 'get',
    params: query
  })
}

// 导出营养师个人资料 Excel
export function exportProfileExcel(query) {
  return request({
    url: '/dietician/profile/export-excel',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}
