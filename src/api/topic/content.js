import request from '@/utils/request'

// 创建内容配置管理
export function createContent(data) {
  return request({
    url: '/topic/content/create',
    method: 'post',
    data: data
  })
}

// 更新内容配置管理
export function updateContent(data) {
  return request({
    url: '/topic/content/update',
    method: 'put',
    data: data
  })
}

// 删除内容配置管理
export function deleteContent(id) {
  return request({
    url: '/topic/content/delete?id=' + id,
    method: 'delete'
  })
}

// 获得内容配置管理
export function getContent(id) {
  return request({
    url: '/topic/content/get?id=' + id,
    method: 'get'
  })
}

// 获得内容配置管理分页
export function getContentPage(query) {
  return request({
    url: '/topic/content/page',
    method: 'get',
    params: query
  })
}

// 导出内容配置管理 Excel
export function exportContentExcel(query) {
  return request({
    url: '/topic/content/export-excel',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}
