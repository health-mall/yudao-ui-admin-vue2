import request from '@/utils/request'

// 创建短视频主题分类
export function createCategory(data) {
  return request({
    url: '/topic/category/create',
    method: 'post',
    data: data
  })
}

// 更新短视频主题分类
export function updateCategory(data) {
  return request({
    url: '/topic/category/update',
    method: 'put',
    data: data
  })
}

// 删除短视频主题分类
export function deleteCategory(id) {
  return request({
    url: '/topic/category/delete?id=' + id,
    method: 'delete'
  })
}

// 获得短视频主题分类
export function getCategory(id) {
  return request({
    url: '/topic/category/get?id=' + id,
    method: 'get'
  })
}

// 获得短视频主题分类分页
export function getCategoryPage(query) {
  return request({
    url: '/topic/category/page',
    method: 'get',
    params: query
  })
}

// 导出短视频主题分类 Excel
export function exportCategoryExcel(query) {
  return request({
    url: '/topic/category/export-excel',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}

// 获得短视频分类主题简单对象列表
export function getCategorySimpleList() {
  return request({
    url: '/topic/category/list-all-simple',
    method: 'get'
  })
}
