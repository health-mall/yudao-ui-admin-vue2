import request from '@/utils/request'

// 获得用户积分记录分页
export function getPointRecordPage(query) {
  return request({
    url: '/member/point/record/page',
    method: 'get',
    params: query
  })
}
