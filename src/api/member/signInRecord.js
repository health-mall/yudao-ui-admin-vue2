import request from '@/utils/request'

// 获得签到记录分页
export function getSignInRecordPage(query) {
  return request({
    url: '/member/sign-in/record/page',
    method: 'get',
    params: query
  })
}
