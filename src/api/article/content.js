import request from '@/utils/request'

// 创建文章内容
export function createContent(data) {
  return request({
    url: '/article/content/create',
    method: 'post',
    data: data
  })
}

// 更新文章内容
export function updateContent(data) {
  return request({
    url: '/article/content/update',
    method: 'put',
    data: data
  })
}

// 删除文章内容
export function deleteContent(id) {
  return request({
    url: '/article/content/delete?id=' + id,
    method: 'delete'
  })
}

// 获得文章内容
export function getContent(id) {
  return request({
    url: '/article/content/get?id=' + id,
    method: 'get'
  })
}

// 获得文章内容分页
export function getContentPage(query) {
  return request({
    url: '/article/content/page',
    method: 'get',
    params: query
  })
}

// 导出文章内容 Excel
export function exportContentExcel(query) {
  return request({
    url: '/article/content/export-excel',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}
