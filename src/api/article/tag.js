import request from '@/utils/request'

// 创建文章标签
export function createTag(data) {
  return request({
    url: '/article/tag/create',
    method: 'post',
    data: data
  })
}

// 更新文章标签
export function updateTag(data) {
  return request({
    url: '/article/tag/update',
    method: 'put',
    data: data
  })
}

// 删除文章标签
export function deleteTag(id) {
  return request({
    url: '/article/tag/delete?id=' + id,
    method: 'delete'
  })
}

// 获得文章标签
export function getTag(id) {
  return request({
    url: '/article/tag/get?id=' + id,
    method: 'get'
  })
}

// 获得文章标签分页
export function getTagPage(query) {
  return request({
    url: '/article/tag/page',
    method: 'get',
    params: query
  })
}

// 导出文章标签 Excel
export function exportTagExcel(query) {
  return request({
    url: '/article/tag/export-excel',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}

// 获得文章标签简单列表
export function getTagSimpleList(categoryId) {
  return request({
    url: '/article/tag/get-simple-list?categoryId=' + categoryId,
    method: 'get'
  })
}

// 获得全部标签的简单列表
export function getAllTagSimpleList() {
  return request({
    url: '/article/tag/get-simple-list-all',
    method: 'get'
  })
}