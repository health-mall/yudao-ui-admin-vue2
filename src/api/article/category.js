import request from '@/utils/request'

// 创建文章分类
export function createCategory(data) {
  return request({
    url: '/article/category/create',
    method: 'post',
    data: data
  })
}

// 更新文章分类
export function updateCategory(data) {
  return request({
    url: '/article/category/update',
    method: 'put',
    data: data
  })
}

// 删除文章分类
export function deleteCategory(id) {
  return request({
    url: '/article/category/delete?id=' + id,
    method: 'delete'
  })
}

// 获得文章分类
export function getCategory(id) {
  return request({
    url: '/article/category/get?id=' + id,
    method: 'get'
  })
}

// 获得文章分类分页
export function getCategoryPage(query) {
  return request({
    url: '/article/category/page',
    method: 'get',
    params: query
  })
}

// 导出文章分类 Excel
export function exportCategoryExcel(query) {
  return request({
    url: '/article/category/export-excel',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}

// 获得文章分类主题简单对象列表
export function getArticleCategorySimpleList() {
  return request({
    // url: '/article/category/get-simple-list',
    url: '/article/category/list-all-simple',
    method: 'get'
  })
}